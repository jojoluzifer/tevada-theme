<?php

// Add Custom Styles with WP wp_add_inline_style Method

function my_styles_method() {
    
    global $data; 
    
    wp_enqueue_style(
		'custom-style',
		get_template_directory_uri() . '/framework/css/custom_script.css'
	);
    
    // ----------- VARIABLES -----------
        //$color_headerbg = $data['color_headerbg'];
        $font_body_face = $data['font_body']['face'];
		$font_body_size = $data['font_body']['size'];
		$font_body_style = $data['font_body']['style'];
		$font_body_color = $data['font_body']['color'];

		$font_h1_face = $data['font_h1']['face'];
		$font_h1_size = $data['font_h1']['size'];
		$font_h1_style = $data['font_h1']['style'];
		$font_h1_color = $data['font_h1']['color'];

		$font_h2_face = $data['font_h2']['face'];
		$font_h2_size = $data['font_h2']['size'];
		$font_h2_style = $data['font_h2']['style'];
		$font_h2_color = $data['font_h2']['color'];

		$font_h3_face = $data['font_h3']['face'];
		$font_h3_size = $data['font_h3']['size'];
		$font_h3_style = $data['font_h3']['style'];
		$font_h3_color = $data['font_h3']['color'];

		$font_h4_face = $data['font_h4']['face'];
		$font_h4_size = $data['font_h4']['size'];
		$font_h4_style = $data['font_h4']['style'];
		$font_h4_color = $data['font_h4']['color'];

		$font_h5_face = $data['font_h5']['face'];
		$font_h5_size = $data['font_h5']['size'];
		$font_h5_style = $data['font_h5']['style'];
		$font_h5_color = $data['font_h5']['color'];

		$font_h6_face = $data['font_h6']['face'];
		$font_h6_size = $data['font_h6']['size'];
		$font_h6_style = $data['font_h6']['style'];
		$font_h6_color = $data['font_h6']['color'];

		$color_link = $data['color_link']; // LINK COLOR
		$color_hover = $data['color_hover']; // HOVER COLOR

		$font_callus_face = $data['font_callus']['face'];
		$font_callus_size = $data['font_callus']['size'];
		$font_callus_style = $data['font_callus']['style'];
		$font_callus_color = $data['font_callus']['color'];

		$select_infobarstatic = $data['select_infobarstatic'];

		$color_topbarbg = $data['color_topbarbg'];
		$border_topbar_width = $data['border_topbar']['width'];
		$border_topbar_style = $data['border_topbar']['style'];
		$border_topbar_color = $data['border_topbar']['color'];
		$color_topbarlinkhover = $data['color_topbarlinkhover'];

		$color_headerbg = $data['color_headerbg'];
		$font_slogan_face = $data['font_slogan']['face'];
		$font_slogan_size = $data['font_slogan']['size'];
		$font_slogan_style = $data['font_slogan']['style'];
		$font_slogan_color = $data['font_slogan']['color'];

		$style_headerheight = $data['style_headerheight'];
		$style_headerheight_minus = $data['style_headerheight']-3;
		$style_logotopmargin = $data['style_logotopmargin'];
		$style_searchformtopmargin = $data['style_searchformtopmargin'];

		$font_nav_face = $data['font_nav']['face'];
		$font_nav_size = $data['font_nav']['size'];
		$font_nav_style = $data['font_nav']['style'];
		$font_nav_color = $data['font_nav']['color'];
		$color_navlinkhover = $data['color_navlinkhover'];
		$color_navlinkactive = $data['color_navlinkactive'];

		$style_hv2height = $data['style_hv2height'];
		$style_hv2logotopmargin = $data['style_hv2logotopmargin'];
		$style_hv2searchformtopmargin = $data['style_hv2searchformtopmargin'];
		$style_hv2slogantopmargin = $data['style_hv2slogantopmargin'];
		$style_hv2submenumargin = $data['style_hv2submenumargin'];

		$style_hv3height = $data['style_hv3height'];
		$style_hv3logotopmargin = $data['style_hv3logotopmargin'];
		$style_hv3navigationtopmargin = $data['style_hv3navigationtopmargin'];
		$style_hv3searchformtopmargin = $data['style_hv3searchformtopmargin'];
		$style_hv3submenumargin = $data['style_hv3submenumargin'];

		$style_hv4height = $data['style_hv4height'];
		$style_hv4logotopmargin = $data['style_hv4logotopmargin'];
		$style_hv4searchformtopmargin = $data['style_hv4searchformtopmargin'];
		$style_hv4slogantopmargin = $data['style_hv4slogantopmargin'];
		$style_hv4submenumargin = $data['style_hv4submenumargin'];
		$color_hv4bgcolor = $data['color_hv4bgcolor'];

		$style_hv5height = $data['style_hv5height'];
		$style_hv5logotopmargin = $data['style_hv5logotopmargin'];
		$style_hv5slogantopmargin = $data['style_hv5slogantopmargin'];
		$style_hv5submenumargin = $data['style_hv5submenumargin'];

		$color_submenubg = $data['color_submenubg'];
		$color_submenuborder = $data['color_submenuborder'];
		$color_submenulink = $data['color_submenulink'];
		$color_submenulinkborder = $data['color_submenulinkborder'];
		$color_submenulinkhover = $data['color_submenulinkhover'];

		$color_titlebgtop = $data['color_titlebgtop'];
		$color_titlebgbottom = $data['color_titlebgbottom'];
		$border_titlebottom_width = $data['border_titlebottom']['width'];
		$border_titlebottom_style = $data['border_titlebottom']['style'];
		$border_titlebottom_color = $data['border_titlebottom']['color'];
		$border_titletop_width = $data['border_titletop']['width'];
		$border_titletop_style = $data['border_titletop']['style'];
		$border_titletop_color = $data['border_titletop']['color'];

		$font_titleh1_face = $data['font_titleh1']['face'];
		$font_titleh1_size = $data['font_titleh1']['size'];
		$font_titleh1_style = $data['font_titleh1']['style'];
		$font_titleh1_color = $data['font_titleh1']['color'];

		$font_titleh2_face = $data['font_titleh2']['face'];
		$font_titleh2_size = $data['font_titleh2']['size'];
		$font_titleh2_style = $data['font_titleh2']['style'];
		$font_titleh2_color = $data['font_titleh2']['color'];

		$color_titlebreadcrumb = $data['color_titlebreadcrumb'];
		$color_titlebreadcrumbhover = $data['color_titlebreadcrumbhover'];

		$color_alttitlebg1 = $data['color_alttitlebg1'];
		$font_alttitleh1_face = $data['font_alttitleh1']['face'];
		$font_alttitleh1_size = $data['font_alttitleh1']['size'];
		$font_alttitleh1_style = $data['font_alttitleh1']['style'];
		$font_alttitleh1_color = $data['font_alttitleh1']['color'];

		$color_alttitlebg2 = $data['color_alttitlebg2'];
		$font_alttitleh2_face = $data['font_alttitleh2']['face'];
		$font_alttitleh2_size = $data['font_alttitleh2']['size'];
		$font_alttitleh2_style = $data['font_alttitleh2']['style'];
		$font_alttitleh2_color = $data['font_alttitleh2']['color'];

		$color_alttitlebreadcrumbbg = $data['color_alttitlebreadcrumbbg'];

		$border_alttitlebreadcrumb_width = $data['border_alttitlebreadcrumb']['width'];
		$border_alttitlebreadcrumb_style = $data['border_alttitlebreadcrumb']['style'];
		$border_alttitlebreadcrumb_color = $data['border_alttitlebreadcrumb']['color'];

		$color_alttitlebreadcrumblink = $data['color_alttitlebreadcrumblink'];
		$color_alttitlebreadcrumblinkhover = $data['color_alttitlebreadcrumblinkhover'];

		$titlebar_gridopacity = $data['titlebar_gridopacity'];

		$font_alttitle2h1_face = $data['font_alttitle2h1']['face'];
		$font_alttitle2h1_size = $data['font_alttitle2h1']['size'];
		$font_alttitle2h1_style = $data['font_alttitle2h1']['style'];
		$font_alttitle2h1_color = $data['font_alttitle2h1']['color'];

		$font_alttitle2h1_face = $data['font_alttitle2h1']['face'];
		$font_alttitle2h1_size = $data['font_alttitle2h1']['size'];
		$font_alttitle2h1_style = $data['font_alttitle2h1']['style'];
		$font_alttitle2h1_color = $data['font_alttitle2h1']['color'];

		$color_alttitle2bg = $data['color_alttitle2bg'];

		$border_alttitle2border_width = $data['border_alttitle2border']['width'];
		$border_alttitle2border_style = $data['border_alttitle2border']['style'];
		$border_alttitle2border_color = $data['border_alttitle2border']['color'];

		$color_alttitle2breadcrumblink = $data['color_alttitle2breadcrumblink'];
		$color_alttitle2breadcrumblinkhover = $data['color_alttitle2breadcrumblinkhover'];

		$font_sidebarwidget_face = $data['font_sidebarwidget']['face'];
		$font_sidebarwidget_size = $data['font_sidebarwidget']['size'];
		$font_sidebarwidget_style = $data['font_sidebarwidget']['style'];
		$font_sidebarwidget_color = $data['font_sidebarwidget']['color'];

		$font_twitter_face = $data['font_twitter']['face'];
		$font_twitter_size = $data['font_twitter']['size'];
		$font_twitter_style = $data['font_twitter']['style'];
		$font_twitter_color = $data['font_twitter']['color'];
		$color_twitterbg = $data['color_twitterbg'];

		$border_footertop_width = $data['border_footertop']['width'];
		$border_footertop_style = $data['border_footertop']['style'];
		$border_footertop_color = $data['border_footertop']['color'];

		$color_footerbg = $data['color_footerbg'];
		$color_footertext = $data['color_footertext'];
		$color_footerlink = $data['color_footerlink'];
		$color_footerlinkhover = $data['color_footerlinkhover'];

		$font_footerheadline_face = $data['font_footerheadline']['face'];
		$font_footerheadline_size = $data['font_footerheadline']['size'];
		$font_footerheadline_style = $data['font_footerheadline']['style'];
		$font_footerheadline_color = $data['font_footerheadline']['color'];

		$border_footerheadline_width = $data['border_footerheadline']['width'];
		$border_footerheadline_style = $data['border_footerheadline']['style'];
		$border_footerheadline_color = $data['border_footerheadline']['color'];

		$color_copyrightbg = $data['color_copyrightbg'];
		$color_copyrighttext = $data['color_copyrighttext'];
		$color_copyrightlink = $data['color_copyrightlink'];
		$color_copyrightlinkhover = $data['color_copyrightlinkhover'];

		$color_accent = $data['color_accent'];

    
    
    // ----------- STYLES -----------
    
    $custom_css = "
        
        /* Header V2 ------------------------------------------------------------------------ */  
        
        #header-v2 #main { height: {$style_hv2height}; }
			#header-v2 .logo{ margin-top: {$style_hv2logotopmargin}; }
                        #header-v2-s #header-v2-searchform {padding-top: {$style_hv2searchformtopmargin};}
        

        ";
    
    // ----------- ADD INLINE STYLES -----------
    
    wp_add_inline_style( 'custom-style', $custom_css );
    
}

add_action( 'wp_enqueue_scripts', 'my_styles_method' );

?>
