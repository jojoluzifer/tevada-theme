<?php

function dekdum_scripts_basic() {

    /* ------------------------------------------------------------------------ */
    /* Register Stylesheets */
    /* ------------------------------------------------------------------------ */

    wp_register_style('bootstrap', get_template_directory_uri() . '/framework/css/bootstrap.css', array(), '1', 'all');
    wp_register_style('font-awesome', get_template_directory_uri() . '/framework/css/font-awesome.css', array(), '1', 'all');


    /* ------------------------------------------------------------------------ */
    /* Enqueue Stylesheets */
    /* ------------------------------------------------------------------------ */


    wp_enqueue_style('bootstrap');
    wp_enqueue_style('font-awesome');



    wp_enqueue_style('stylesheet', get_stylesheet_uri(), array(), '1', 'all'); // Main Stylesheet



    global $data;

    if ($data['check_responsive'] == true) {
//		wp_enqueue_style( 'skeleton' );
        wp_enqueue_style('responsive');
    }
}

add_action('wp_enqueue_scripts', 'dekdum_scripts_basic', 1);

function dekdum_styles_basic() {
    /* ------------------------------------------------------------------------ */
    /* Register Stylesheets */
    /* ------------------------------------------------------------------------ */

    wp_register_style('headers', get_template_directory_uri() . '/framework/css/headers.css', array(), '1', 'all');
    


    /* ------------------------------------------------------------------------ */
    /* Enqueue Stylesheets */
    /* ------------------------------------------------------------------------ */

    wp_enqueue_style('headers');
    

    wp_enqueue_style('stylesheet', get_stylesheet_uri(), array(), '1', 'all'); // Main Stylesheet

    global $data;

    if ($data['check_responsive'] == true) {
//		wp_enqueue_style( 'skeleton' );
        wp_enqueue_style('responsive');
    }
}

add_action('wp_enqueue_scripts', 'dekdum_styles_basic', 1);
?>