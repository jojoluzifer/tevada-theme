<header id="header-v2">

    <?php if ($data['check_topbar'] == true) { ?>
        <div id="topbar" class="clearfix <?php
    if ($data['check_socialtopbar'] == false) {
        echo 'no-social';
    }
        ?>">

            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <?php if ($data['check_socialtopbar'] == true) { ?>
                            <div class="social-icons">
                                <ul>
                                    <?php if ($data['social_twitter'] != "") { ?>
                                        <li class="social-twitter"><a href="http://www.twitter.com/<?php echo $data['social_twitter']; ?>" target="_blank" title="<?php _e('Twitter', 'dekdum') ?>"><?php _e('Twitter', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_forrst'] != "") { ?>
                                        <li class="social-forrst"><a href="<?php echo $data['social_forrst']; ?>" target="_blank" title="<?php _e('Forrst', 'dekdum') ?>"><?php _e('Forrst', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_dribbble'] != "") { ?>
                                        <li class="social-dribbble"><a href="<?php echo $data['social_dribbble']; ?>" target="_blank" title="<?php _e('Dribbble', 'dekdum') ?>"><?php _e('Dribbble', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_flickr'] != "") { ?>
                                        <li class="social-flickr"><a href="<?php echo $data['social_flickr']; ?>" target="_blank" title="<?php _e('Flickr', 'dekdum') ?>"><?php _e('Flickr', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_facebook'] != "") { ?>
                                        <li class="social-facebook"><a href="<?php echo $data['social_facebook']; ?>" target="_blank" title="<?php _e('Facebook', 'dekdum') ?>"><?php _e('Facebook', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_skype'] != "") { ?>
                                        <li class="social-skype"><a href="<?php echo $data['social_skype']; ?>" target="_blank" title="<?php _e('Skype', 'dekdum') ?>"><?php _e('Skype', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_digg'] != "") { ?>
                                        <li class="social-digg"><a href="<?php echo $data['social_digg']; ?>" target="_blank" title="<?php _e('Digg', 'dekdum') ?>"><?php _e('Digg', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_google'] != "") { ?>
                                        <li class="social-google"><a href="<?php echo $data['social_google']; ?>" target="_blank" title="<?php _e('Google', 'dekdum') ?>"><?php _e('Google+', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_instagram'] != "") { ?>
                                        <li class="social-instagram"><a href="<?php echo $data['social_instagram']; ?>" target="_blank" title="<?php _e('Instagram', 'dekdum') ?>"><?php _e('Instagram', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_linkedin'] != "") { ?>
                                        <li class="social-linkedin"><a href="<?php echo $data['social_linkedin']; ?>" target="_blank" title="<?php _e('LinkedIn', 'dekdum') ?>"><?php _e('LinkedIn', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_vimeo'] != "") { ?>
                                        <li class="social-vimeo"><a href="<?php echo $data['social_vimeo']; ?>" target="_blank" title="<?php _e('Vimeo', 'dekdum') ?>"><?php _e('Vimeo', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_yahoo'] != "") { ?>
                                        <li class="social-yahoo"><a href="<?php echo $data['social_yahoo']; ?>" target="_blank" title="<?php _e('Yahoo', 'dekdum') ?>"><?php _e('Yahoo', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_tumblr'] != "") { ?>
                                        <li class="social-tumblr"><a href="<?php echo $data['social_tumblr']; ?>" target="_blank" title="<?php _e('Tumblr', 'dekdum') ?>"><?php _e('Tumblr', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_youtube'] != "") { ?>
                                        <li class="social-youtube"><a href="<?php echo $data['social_youtube']; ?>" target="_blank" title="<?php _e('YouTube', 'dekdum') ?>"><?php _e('YouTube', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_picasa'] != "") { ?>
                                        <li class="social-picasa"><a href="<?php echo $data['social_picasa']; ?>" target="_blank" title="<?php _e('Picasa', 'dekdum') ?>"><?php _e('Picasa', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_deviantart'] != "") { ?>
                                        <li class="social-deviantart"><a href="<?php echo $data['social_deviantart']; ?>" target="_blank" title="<?php _e('DeviantArt', 'dekdum') ?>"><?php _e('DeviantArt', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_behance'] != "") { ?>
                                        <li class="social-behance"><a href="<?php echo $data['social_behance']; ?>" target="_blank" title="<?php _e('Behance', 'dekdum') ?>"><?php _e('Behance', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_pinterest'] != "") { ?>
                                        <li class="social-pinterest"><a href="<?php echo $data['social_pinterest']; ?>" target="_blank" title="<?php _e('Pinterest', 'dekdum') ?>"><?php _e('Pinterest', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_paypal'] != "") { ?>
                                        <li class="social-paypal"><a href="<?php echo $data['social_paypal']; ?>" target="_blank" title="<?php _e('PayPal', 'dekdum') ?>"><?php _e('PayPal', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_delicious'] != "") { ?>
                                        <li class="social-delicious"><a href="<?php echo $data['social_delicious']; ?>" target="_blank" title="<?php _e('Delicious', 'dekdum') ?>"><?php _e('Delicious', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_xing'] != "") { ?>
                                        <li class="social-xing"><a href="<?php bloginfo('social_xing'); ?>" target="_blank" title="<?php _e('XING', 'dekdum') ?>"><?php _e('XING', 'dekdum') ?></a></li>
                                    <?php } ?>
                                    <?php if ($data['social_rss'] == true) { ?>
                                        <li class="social-rss"><a href="<?php bloginfo('rss2_url'); ?>" target="_blank" title="<?php _e('RSS', 'dekdum') ?>"><?php _e('RSS', 'dekdum') ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-xs-6">
                        <div class="slogan"><?php echo pre_process_shortcode($data['text_hv2slogan']); ?></div>
                        <?php if ($data['check_searchform'] == true) { ?>
                            <div  id="header-v2-s">
                                <form action="<?php echo home_url(); ?>/" id="header-v2-searchform" method="get" >
                                    <input type="text"  name="s" value="" autocomplete="off" />
                                    <span class="glyphicon glyphicon-search"></span> 

                                </form>
                            </div>
                            <div class="clearfix"></div>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div> <!-- end topbar -->
    <?php } ?>

    <div class="container" id="main">
        <div class="row">
            <div class="col-xs-3">
                <div class="logo">
                    <?php if ($data['media_logo'] != "") { ?>
                        <a href="<?php echo home_url(); ?>/"><img src="<?php echo $data['media_logo']; ?>" alt="<?php bloginfo('name'); ?>" class="logo_standard" /></a>
                        <?php if ($data['media_logo_retina'] != '') { ?><a href="<?php echo home_url(); ?>/"><img src="<?php echo $data['media_logo_retina'] ?>" width="<?php echo $data['logo_width']; ?>" height="<?php echo $data['logo_height']; ?>" alt="<?php bloginfo('name'); ?>" class="logo_retina" /></a><?php } ?>
                    <?php } else { ?>
                        <a href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xs-9">
                <div class="navigation" >
                    <?php wp_nav_menu(array('theme_location' => 'main_navigation', 'menu_id' => 'nav')); ?>
                </div>
            </div>
        </div>
    </div>



</header>