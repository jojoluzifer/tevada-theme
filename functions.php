<?php


/* ------------------------------------------------------------------------ */
/* Inlcudes
/* ------------------------------------------------------------------------ */


        /* ------------------------------------------------------------------------ */
	/* Misc Includes */

	include_once('framework/inc/enqueue.php'); // Enqueue JavaScripts & CSS
        include_once('framework/inc/customcss.php'); // Load custom CSS
        include_once('framework/inc/shortcodes.php'); // Load Shortcodes

	/* ------------------------------------------------------------------------ */
	/* Include SMOF */
	require_once('admin/index.php'); // Slightly Modified Options Framework
	
	/* ------------------------------------------------------------------------ */