<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
    <head>

        <!-- Basic Page Needs 
        ========================================================= -->
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php bloginfo('name'); ?> <?php wp_title(' - ', true, 'left'); ?></title>

        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <?php global $data; ?>

        <!-- Mobile Specific Metas & Favicons
        ========================================================= -->
        <?php if ($data['check_mobilezoom'] == 0) { ?><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"><?php } ?>

        <?php if ($data['media_favicon'] != "") { ?><link rel="shortcut icon" href="<?php echo $data['media_favicon']; ?>"><?php } ?>

        <?php if ($data['media_favicon_iphone'] != "") { ?><link rel="apple-touch-icon" href="<?php echo $data['media_favicon_iphone']; ?>"><?php } ?>

        <?php if ($data['media_favicon_iphone_retina'] != "") { ?><link rel="apple-touch-icon" sizes="114x114" href="<?php echo $data['media_favicon_iphone_retina']; ?>"><?php } ?>

        <?php if ($data['media_favicon_ipad'] != "") { ?><link rel="apple-touch-icon" sizes="72x72" href="<?php echo $data['media_favicon_ipad']; ?>"><?php } ?>

        <?php if ($data['media_favicon_ipad_retina'] != "") { ?><link rel="apple-touch-icon" sizes="144x144" href="<?php echo $data['media_favicon_ipad_retina']; ?>"><?php } ?>


        <!-- WordPress Stuff
        ========================================================= -->
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>

        <?php wp_head(); ?>

    </head>

    <body <?php body_class(); ?>>

        <?php if ($data['select_layoutstyle'] == 'Boxed Layout' || $data['select_layoutstyle'] == 'Boxed Layout with margin') { ?>	
            <div id="boxed-layout">

                test

            <?php } ?>

            <?php
            if ($data['header_layout']) {
                if (is_page('header-2')) {
                    include_once('framework/inc/headers/header-v2.php');
                } elseif (is_page('header-3')) {
                    include_once('framework/inc/headers/header-v3.php');
                } elseif (is_page('header-4')) {
                    include_once('framework/inc/headers/header-v4.php');
                } elseif (is_page('header-5')) {
                    include_once('framework/inc/headers/header-v5.php');
                } else {
                    include_once('framework/inc/headers/header-' . $data['header_layout'] . '.php');
                }
            } else {
                if (is_page('header-2')) {
                    include_once('framework/inc/headers/header-v2.php');
                } elseif (is_page('header-3')) {
                    include_once('framework/inc/headers/header-v3.php');
                } elseif (is_page('header-4')) {
                    include_once('framework/inc/headers/header-v4.php');
                } elseif (is_page('header-5')) {
                    include_once('framework/inc/headers/header-v5.php');
                } else {
                    include_once('framework/inc/headers/header-v1.php');
                }
            }
            ?>